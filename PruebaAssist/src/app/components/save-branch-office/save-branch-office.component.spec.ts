import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaveBranchOfficeComponent } from './save-branch-office.component';

describe('SaveBranchOfficeComponent', () => {
  let component: SaveBranchOfficeComponent;
  let fixture: ComponentFixture<SaveBranchOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaveBranchOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaveBranchOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
