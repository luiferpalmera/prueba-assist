import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { Subject } from 'rxjs';
import { BranchOffice } from 'src/app/models/BranchOffice.model';
import { BranchOfficeService } from 'src/app/services/branch-offices-services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-branch-office',
  templateUrl: './branch-office.component.html',
  styleUrls: ['./branch-office.component.css']
})
export class BranchOfficeComponent implements OnDestroy,OnInit {

  errores:object[]=[];
  branchOffices:BranchOffice[] = [];
  dtOptions: DataTables.Settings = {};

  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject<any>();


  constructor(private _branchOfficeService:BranchOfficeService,private spinner: NgxSpinnerService) {

  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.spinner.show();
    this._branchOfficeService.getBranchOffices().subscribe(data => {
      this.branchOffices = data;
      this.dtTrigger.next(null);
      this.spinner.hide();
    });

  }

  eliminarEmpleado(branchOffice:BranchOffice){}

  mostrarConfirmacionEliminar(branchOffice:BranchOffice){
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
      title: '¿Seguro que desea eliminar el registro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, borrarlo!',
      cancelButtonText: 'No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.isConfirmed) {
        this._branchOfficeService.deleteBranchOffice(branchOffice.id).subscribe(data => {
          swalWithBootstrapButtons.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          )
          this.branchOffices[this.branchOffices.indexOf(branchOffice)].status = !this.branchOffices[this.branchOffices.indexOf(branchOffice)].status;
          this.spinner.hide();
        });

      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          'Cancelled',
          'Your imaginary file is safe :)',
          'error'
        )
      }
    })
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

}
