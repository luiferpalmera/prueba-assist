export interface BranchOffice{
  id?:number;
  code:number;
  description:string;
  address:string;
  identification:string;
  createdAt:string;
  money:string;
  status:boolean;
}
